<?php

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ModuleController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\QuestionController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AnswerController;


Route::get('/', function () {
    return view('welcome');
});

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
    Route::get('/dashboard', DashboardController::class)->name('dashboard');
    Route::post('/profile', [ProfileController::class, 'storeModules'])->name('profile.store.modules');
    Route::resource('modules', ModuleController::class);
    Route::resource('question', QuestionController::class)->except('create');
    Route::get('/question/create/{module_id}',[QuestionController::class,'create'])->name('question.create');
    Route::post('/question/update/{answer}',[AnswerController::class,'update'])->name('answer.update');
    Route::get("/question/{question}/answer", [AnswerController::class, 'create'])->name('answer.question');
    Route::post("/answer", [AnswerController::class, 'store'])->name('answer.store');
    Route::get("/questions-pending/module/{module_id}", [QuestionController::class,'pending'])->name('question.pending');
});


require __DIR__.'/auth.php';

