<?php
use App\Http\Controllers\Api\QuestionController;
use App\Http\Controllers\Api\AnswerController;
use App\Http\Controllers\Api\ModuleController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\LoginController;

// RUTAS PROTEGIDAS
Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:sanctum');
Route::apiResource('questions',QuestionController::class)->middleware('auth:sanctum');
Route::apiResource('answers',AnswerController::class)->middleware('auth:sanctum');
Route::apiResource('modules',ModuleController::class)->middleware('auth:sanctum');

Route::get('showPendingQuestions',[QuestionController::class, 'showPending']);
Route::post('login', [LoginController::class, 'login']);
