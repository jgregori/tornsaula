<div class="hidden space-x-8 sm:-my-px sm:ms-10 sm:flex">
    <x-nav-link :href="route('question.index')" :active="request()->routeIs('question.index')">
        {{ __("My questions") }}
    </x-nav-link>
</div>

<div class="hidden sm:flex sm:items-center sm:ms-6">

    @foreach($modules as $module)
        <x-dropdown align="right" width="48">
            <div class="hidden space-x-8 sm:-my-px sm:ms-10 sm:flex">
                <x-slot name="trigger">
                    <button class="px-2 dark:text-white text-black">
                        {{ __($module->name) }}
                    </button>
                </x-slot>

                <x-slot name="content">
                    <x-dropdown-link :href="route('question.show', $module->id)">
                        {{ __('All') }}
                    </x-dropdown-link>

                    <x-dropdown-link :href="route('question.pending', $module->id)">
                        {{ __('Pending') }}
                    </x-dropdown-link>
                </x-slot>
            </div>
        </x-dropdown>
    @endforeach
</div>



