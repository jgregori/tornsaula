<div>
    <textarea name="{{$name}}" class="mt-1 block w-full">{{$slot}}</textarea>
</div>
