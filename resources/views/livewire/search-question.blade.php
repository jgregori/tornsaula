<div>
    <input type="text" wire:model.live.debounce.200ms="search" placeholder="Search question...">
    <ul class="text-white">
        @foreach ($questions as $question)
            <li>{{$question->title}}</li>
        @endforeach
    </ul>
</div>
