<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __($title) }}
        </h2>
        @if(Auth::user()->role === "user_role")
            @isset($module_id)
                <x-nav-link :href="route('question.create',$module_id)" :active="request()->routeIs('question.create')">
                    {{ __('New question') }}
                </x-nav-link>
            @endisset
        @endif

        <livewire:search-question :areMyQuestions="$areMyQuestions" :module_id="$module_id" :pending="$pending"></livewire:search-question>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900 dark:text-gray-100">
                    <div class="p-6 text-gray-900 dark:text-gray-100">
                        <ul>
                            @if(count($myQuestions) === 0)
                                <p>Por el momento no hay niguna cuestión</p>
                            @endif
                            @foreach($myQuestions as $question)
                                    @php($userId = Auth::user()->id)
                                <li class="mb-6">
                                    <h4 class="text-2xl font-bold dark:text-white">{{$question->title}}</h4>
                                    <div>
                                        <small class="block text-sm">Pregunta realizar por:
                                            @if($question->user_id === $userId)
                                                Yo
                                            @else
                                                {{$question->user->name}}
                                            @endif
                                        </small>
                                        <p class="ml-3 block text-lg">{{$question->description}}</p>
                                        <img class="mx-auto rounded-lg" src="{{asset('storage/'.$question->photo)}}" alt="img de la consulta">
                                        @isset($question->answer->content)
                                            <small class="block text-sm">Ha respondido:
                                                {{$question->answer->user_id === $userId ? "Yo" : $question->answer->user->name}}
                                            </small>
                                            <blockquote class="ml-3 text-base">{{$question->answer->content}}</blockquote>
                                        @else
                                            <blockquote class="ml-3">Sin respuesta</blockquote>
                                        @endif

                                        @if($question->user_id === $userId)
                                            <form action="{{ route("question.destroy", $question->id) }}" method="POST" class="mt-2">
                                                @csrf
                                                @method("DELETE")
                                                <button type="submit" class="bg-red-600 p-2 rounded-md">
                                                    <p>Delete</p>
                                                </button>
                                            </form>
                                        @endif
                                        @if(Auth::user()->role === "teacher_role")
                                            <a href="{{route("answer.question", $question)}}" class="mt-2">
                                                <x-primary-button class="p-2 rounded-md" type="button">
                                                    @isset($question->answer->content)
                                                        Editar respuesta
                                                    @else
                                                        Responder
                                                    @endif
                                                </x-primary-button>
                                            </a>
                                        @endif

                                    </div>
                                </li>
                            @endforeach
                        </ul>
                        {{$myQuestions->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
