
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __($question->title) }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6">
            <div class="p-4 sm:p-8 bg-white dark:bg-gray-800 shadow sm:rounded-lg">
                <div class="max-w-xl">

                    <form method="post" action="{{ isset($answer)? route('answer.update', $answer) : route('answer.store')  }}" class="mt-6 space-y-6"
                          enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="question_id" value="{{$question->id}}" />
                        <input type="hidden" name="user_id" value="{{Auth::user()->id}}" />
                        <div>
                            <x-input-label for="title" :value="__('Title')" />
                            <x-text-input id="title" name="title" type="text" value="{{$question->title}}" class="mt-1 block w-full" required autofocus autocomplete="title" disabled/>
                            <x-input-error class="mt-2" :messages="$errors->get('title')" />
                        </div>

                        <div>
                            <x-input-label for="description" :value="__('Description')" />
                            <x-text-input id="description" name="description" type="text" value="{{$question->description}}" class="mt-1 block w-full" required disabled/>
                            <x-input-error class="mt-2" :messages="$errors->get('description')" />
                        </div>

                        <div>
                            <x-input-label for="photo" :value="__('Photo')" />
                            <x-text-input id="photo" name="photo" type="file" value="{{$question->photo}}" class="mt-1 block w-full" disabled />
                            <x-input-error class="mt-2" :messages="$errors->get('photo')" />
                        </div>

                        <div>
                            <x-input-label for="content" :value="__('Content')" />
                            <x-text-area name="content"> {{$answer->content??""}}</x-text-area>
                            <x-input-error class="mt-2" :messages="$errors->get('content')" />
                        </div>

                        <div class="flex items-center gap-4">
                            <x-primary-button>{{ __('Save') }}</x-primary-button>
                        </div>
                    </form>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                </div>
            </div>
        </div>
    </div>
</x-app-layout>

