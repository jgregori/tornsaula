<?php

namespace App\View\Components;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Checkboxs extends Component
{
    public string $name;

    public array $options;

    public array $modulesSelected;

    public function __construct(string $name, array $options, array $modulesSelected)
    {
        $this->name = $name;
        $this->options = $options;
        $this->modulesSelected = $modulesSelected;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.checkboxs');
    }
}
