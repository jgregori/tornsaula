<?php

namespace App\Livewire;

use App\Models\Question;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class SearchQuestion extends Component
{
    public $search;

    public $areMyQuestions;

    public $module_id;

    public $pending;
    public function render()
    {
        $questions = [];
        $user_id = Auth::user()->id;
        if ($this->areMyQuestions) {
            $questions = $this->searchInMyQuestions($user_id);
        } else {
            if ($this->pending) {
                $questions = $this->searchPendingQuestions();
            } else {
                $questions = $this->searchAllQuestionsInModule();
            }
        }
        return view('livewire.search-question', compact('questions'));
    }

    private function searchInMyQuestions($user_id)
    {
        $questions = Question::where("user_id", "=", $user_id)->orderBy("created_at", "desc")->get();
        if ($this->search) {
            $questions = Question::search($this->search)->raw()['hits'];
            $ids = array_column($questions, 'id');
            $questions = Question::whereIn('id', $ids)->where('user_id', $user_id)->orderByDesc('created_at')->get();
        }
        return $questions;
    }

    private function searchPendingQuestions()
    {
        return Question::whereDoesntHave('answer')->where('module_id', "=", $this->module_id)->orderBY('created_at')->get();
    }

    private function searchAllQuestionsInModule()
    {
        return Question::where("module_id", "=", $this->module_id)->orderBy("created_at", "desc")->get();
    }
}
