<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Laravel\Scout\Searchable;
/**
 *
 * @OA\Schema(
 * required={"password"},
 * @OA\Xml(name="Question"),
 * @OA\Property(property="id", type="integer", example="1"),
 * @OA\Property(property="title", type="string", description="Question title", example="¿Is laravel a framework?"),
 * @OA\Property(property="description", type="string", description="Question description", example="I'm wondering if laravel is a framework or a language"),
 * @OA\Property(property="photo", type="string", description="Photo about question", example="img/5fJgbx4qSU.png"),
 * @OA\Property(property="module_id", type="integer", example="4"),
 * @OA\Property(property="user_id", type="integer", example="2")
 * )
*/
class Question extends Model
{
    use HasFactory;
    use Searchable;

    protected $guarded = [
        'id',
    ];

    public function answer(): HasOne
    {
        return $this->hasOne(Answer::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class)->withDefault(); // withDefault se usa por si no encuentra
    }
}
