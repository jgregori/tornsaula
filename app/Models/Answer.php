<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 *
 * @OA\Schema(
 * required={"password, question_id, user_id, id"},
 * @OA\Xml(name="Answer"),
 * @OA\Property(property="id", type="integer", example="1"),
 * @OA\Property(property="question_id", type="integer", example="1"),
 * @OA\Property(property="user_id", type="integer", example="1"),
 * @OA\Property(property="content", type="string", description="Question answer", example="Laravel is a framework"),
 * )
 */

class Answer extends Model
{
    use HasFactory;
    protected $fillable = [
        "question_id",
        "content",
        "user_id"
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class)->withDefault(); // withDefault se usa por si no encuentra
    }
}
