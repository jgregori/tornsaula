<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 *
 * @OA\Schema(
 * required={"password, id, name, description"},
 * @OA\Xml(name="Module"),
 * @OA\Property(property="id", type="integer", example="1"),
 * @OA\Property(property="name", type="string", description="Short name of the module", example="DIW"),
 * @OA\Property(property="description", type="string", description="Full name of the module",example="Diseña de Interfaces Web"),
 * )
 */

class Module extends Model
{

    public $timestamps = false;

    use HasFactory;

    protected $guarded = ['id'];
}
