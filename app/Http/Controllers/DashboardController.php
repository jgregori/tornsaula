<?php

namespace App\Http\Controllers;

use App\Models\Question;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke()
    {
        $idModules = $this->getModulesIdUser();
        $questions = Question::whereDoesntHave("answer")->whereIn("module_id",$idModules)->orderBy('created_at')->get();
        return view("dashboard", compact("questions"));
    }

    private function getModulesIdUser()
    {
        $modules = Auth::user()->Modules->select('id');
        $idModules = [];
        foreach ($modules as $module){
            $idModules[] = $module;
        }
        return $idModules;
    }
}
