<?php

namespace App\Http\Controllers;

use App\Models\Module;
use App\Models\Question;
use Dotenv\Exception\ValidationException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;



class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $user_id = Auth::user()->id;
        $myQuestions = Question::where("user_id", "=", $user_id)->orderBy("created_at", "desc")->paginate(10);
        $title = "My questions";
        $areMyQuestions = true;
        $module_id = null;
        $pending = false;
        return view("questions.index", compact("myQuestions",  "title", "areMyQuestions", "module_id", "pending"));
    }

    public function pending($module_id)
    {
        $title = ucfirst(Module::find($module_id)->name);

        $myQuestions = Question::whereDoesntHave('answer')->where('module_id', "=", $module_id)->orderBY('created_at')->paginate(10);
        $areMyQuestions = false;
        $pending = true;
        return view('questions.index', compact('myQuestions', 'title', 'module_id', 'areMyQuestions', 'pending'));

    }

    /**
     * Show the form for creating a new resource.
     */
    public function create($module_id)
    {
        return view("questions.create",compact('module_id'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try {
            $validatedData = $request->validate([
                "module_id" => "required|exists:modules,id",
                "title" => "required|string|max:255",
                "description" => "required|string",
                "photo" => "nullable|mimes:jpg,bmp,png",
            ]);
            $user_id = Auth::user()->id;
            $validatedData["user_id"] = $user_id;
            if ($request->has("photo")) {
                $extension = $request->photo->extension();
                $path = $request->photo->storeAs("img", substr(str_replace(["+", "/", "="],  "", base64_encode(random_bytes(10))), 0, 10) . "." . $extension,'public');
                $validatedData["photo"] = $path;
            }
            Question::create($validatedData);
            return back();
        } catch (ValidationException $e) {
            return $e->getMessage();
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(int $module_id)
    {
        $myQuestions = Question::where("module_id", "=", $module_id)->orderBy("created_at", "desc")->paginate(10);
        $title = Module::find($module_id)->name;
        $areMyQuestions = false;
        $pending = false;
        return view("questions.index", compact("myQuestions", "title", "module_id", "areMyQuestions", 'pending'));

    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Question $question)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Question $question)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Question $question)
    {
        $question->delete();
        return back();
    }
}
