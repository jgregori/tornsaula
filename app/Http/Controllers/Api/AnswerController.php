<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Answer;
use Illuminate\Http\Request;

/**
 * @OA\Post(
 * path="/api/answers",
 * summary="Create a new answer",
 * description="Create a new answer",
 * security={ {"apiAuth": {} }},
 * operationId="AnswerStore",
 * tags={"answers"},
 * @OA\RequestBody(
 *    required=true,
 *    description="Create a new answer",
 *    @OA\JsonContent(
 *         ref="#/components/schemas/Answer"
 *      )
 * ),
 * @OA\Response(
 *    response=422,
 *    description="Wrong credentials response",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
 *        )
 *     ),
 * @OA\Response(
 *     response=200,
 *     description="Question create successfully",
 *     @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Answer create!")
 *        )
 *     ),
 * )
 *
 *
 *
 * @OA\Delete(
 *  path="/api/answers",
 *  summary="Delete an existing answer",
 *  description="Delete an existing answer",
 *  security={ {"apiAuth": {} }},
 *  operationId="AnswerDestroy",
 *  tags={"answers"},
 *  @OA\Response(
 *       response=401,
 *       description="Error: Unauthorized",
 *       @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Unauthenticated")
 *           )
 *  ),
 *  @OA\Response(
 *       response=204,
 *       description="Asnwer deleted successfully"
 *   )
 * )
 */

class AnswerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $answers = Answer::get();
        return $answers;
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request-> validate([
            'question_id' => 'required|integer|exists:questions,id',
            'user_id' => 'required|integer|exists:users,id',
            'content' => 'required|string|max:255'
        ]);

        $answer = new Answer();
        $answer->question_id = $request->question_id;
        $answer->user_id = $request->user_id;
        $answer->content = $request->content;
        $answer->save();
        return response()->json($answer, 201);
    }

    /**
     * Display the specified resource.
     */
    public function show(Answer $answer)
    {
        return response()->json($answer, 200);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Answer $answer)
    {
        $answer->question_id = $request->question_id;
        $answer->user_id = $request->user_id;
        $answer->content = $request->content;
        $answer->save();
        return response()->json($request);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Answer $answer)
    {
        $answer->delete();
        return response()->json(null, 204);
    }
}
