<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Module;
use Illuminate\Http\Request;

/**
 * @OA\Post(
 * path="/api/modules",
 * summary="Create a new module",
 * description="Create a new module",
 * security={ {"apiAuth": {} }},
 * operationId="ModuleStore",
 * tags={"modules"},
 * @OA\RequestBody(
 *    required=true,
 *    description="Create a new module",
 *    @OA\JsonContent(
 *         ref="#/components/schemas/Module"
 *      )
 * ),
 * @OA\Response(
 *    response=422,
 *    description="Wrong credentials response",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
 *        )
 *     ),
 * @OA\Response(
 *     response=200,
 *     description="Question create successfully",
 *     @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Module create!")
 *        )
 *     ),
 * )
 *
 *
 * @OA\Delete(
 *   path="/api/modules",
 *   summary="Delete an existing module",
 *   description="Delete an existing module",
 *   security={ {"apiAuth": {} }},
 *   operationId="ModuleDestroy",
 *   tags={"modules"},
 *   @OA\Response(
 *        response=401,
 *        description="Error: Unauthorized",
 *        @OA\JsonContent(
 *        @OA\Property(property="message", type="string", example="Unauthenticated")
 *            )
 *   ),
 *   @OA\Response(
 *        response=204,
 *        description="Module deleted successfully"
 *    )
 *  )
 */

class ModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $module = Module::get();
        return $module;
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'description' => 'required|string|max:255'
        ]);
        $module = new Module();
        $module->name = $request->name;
        $module->description = $request->description;
        $module->save();
        return response()->json($request, 201);
    }

    /**
     * Display the specified resource.
     */
    public function show(Module $module)
    {
        return response()->json($module,200);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Module $module)
    {
        $module->name = $request->name;
        $module->description = $request->description;
        $module->save();
        return response()->json($request);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Module $module)
    {
        $module->delete();
        return response()->json(null, 204);
    }
}
