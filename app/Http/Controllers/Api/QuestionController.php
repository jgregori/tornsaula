<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Question;
use Illuminate\Http\Request;

/**
 * @OA\Post(
 * path="/api/questions",
 * summary="Create a new question",
 * description="Create a new question",
 * security={ {"apiAuth": {} }},
 * operationId="QuestionStore",
 * tags={"questions"},
 * @OA\RequestBody(
 *    required=true,
 *    description="Create a new question",
 *    @OA\JsonContent(
 *         ref="#/components/schemas/Question"
 *      )
 * ),
 * @OA\Response(
 *    response=422,
 *    description="Wrong credentials response",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
 *        )
 *     ),
 * @OA\Response(
 *     response=200,
 *     description="Question create successfully",
 *     @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Question create!")
 *        )
 *     ),
 * )
 *
 */

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $questions = Question::get();
        return $questions;
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request-> validate([
            'title' => 'required|string|max:255',
            'description' => 'required|string|max:255',
            /*'photo' => 'nullable|mimes:jpg,bmp,png,',*/
            'module_id' => 'required|integer|exists:modules,id',
            'user_id' => 'required|integer|exists:users,id'
        ]);

        $question = new Question();
        $question->title = $request->title;
        $question->description = $request->description;
        $question->photo = $request->photo;
        $question->module_id = $request->module_id;
        $question->user_id = $request->user_id;
        $question->save();
        return response()->json($question, 201);
    }

    /**
     * Display the specified resource.
     */
    public function show(Question $question)
    {
        return response()->json($question, 200);
    }

    public function showPending(Question $question)
    {
        return response()->json($question, 200);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Question $question)
    {
        $question->title = $request->title;
        $question->description = $request->description;
        $question->photo = $request->photo;
        $question->module_id = $request->module_id;
        $question->user_id = $request->user_id;
        $question->save();
        return response()->json($request);

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Question $question)
    {
        $question->delete();
        return response()->json(null, 204);
    }
}
