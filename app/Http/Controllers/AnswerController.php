<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use App\Models\Question;
use Dotenv\Exception\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class AnswerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Question $question)
    {
        $answer = Answer::where("question_id", "=", $question->id)->first();
        return view("answer.create", compact("question", "answer"));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        Answer::create($request->toArray());
        return view("dashboard");
        /*
        try {
            $validateData = $request->validate([
                "question_id" => "required|exists:modules,id",
                "user_id" => "required|exists:users,id",
                "content" => "required|string"
            ]);

            Answer::create($validateData);
        } catch (ValidationException $e) {

        }*/
    }

    /**
     * Display the specified resource.
     */
    public function show(Answer $answer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Answer $answer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Answer $answer)
    {
        try {
            $validatedData = $request->validate([
                "question_id" => "required|exists:questions,id",
                "content" => "required|string|max:255",
            ]);
            $answer->update($validatedData);
            return view("dashboard");
        } catch (ValidationException $e) {
            return $e->errors();
        } catch (ModelNotFoundException $e) {
            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Answer $answer)
    {
        //
    }
}
